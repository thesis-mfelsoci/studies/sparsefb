(require 'org) ;; Org mode support
(require 'htmlize) ;; source code block export to HTML
(require 'ox-publish) ;; publishing functions
(require 'oc)
(require 'citeproc) ;; for HTML
(require 'oc-csl) ;; for HTML

;; Enable Babel code evaluation.
(setq org-export-babel-evaluate nil)

;; Load style presets.
(load-file "styles/styles.el")

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "site"
             :base-directory "."
             :exclude ".*"
             :include [;"reproducing-guidelines.org"
                       "study.org"]
             :base-extension "org"
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public")
       (list "figures"
             :base-directory "./figures"
             :base-extension "png\\|jpg\\|gif\\|svg\\|ico"
             :recursive t
             :publishing-directory "./public/figures"
             :publishing-function '(org-publish-attachment))
       (list "study"
             :components '("site"))
       (list "study-static"
             :components '("figures"))))

(provide 'publish)
