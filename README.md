# test_FEMBEM: Advanced setup

[![pipeline status](https://gitlab.inria.fr/tuto-techno-guix-hpc/test_fembem/advanced-setup/badges/master/pipeline.svg)](https://gitlab.inria.fr/tuto-techno-guix-hpc/test_fembem/advanced-setup/-/commits/master)

This repository contains a copy of an experimental study relying on the
open-source version of the `test_FEMBEM` solver test suite [3]. In this case, we
**do rely** rely on the GNU Guix [1] transactional package manager and literate
programming [2] to ensure reproducibility of the research study. We refer to
this version of the study as to *Advanced setup*.

## Where to find the original study?

You can find all the materials and reproducing guidelines
[here](https://tuto-techno-guix-hpc.gitlabpages.inria.fr/test_fembem/advanced-setup).

## References

1. GNU Guix software distribution and transactional package manager
   [https://guix.gnu.org](https://guix.gnu.org).
2. Literate Programming, Donald E. Knuth, 1984
   [https://doi.org/10.1093/comjnl/27.2.97](https://doi.org/10.1093/comjnl/27.2.97).
3. test_FEMBEM, a simple application for testing dense and sparse solvers with
   pseudo-FEM or pseudo-BEM matrices
   [https://gitlab.inria.fr/solverstack/test_fembem](https://gitlab.inria.fr/solverstack/test_fembem).
