* Defining benchmarks
:PROPERTIES:
:CUSTOM_ID: definition-file
:END:

Within the experimental part of the study, we want to run the benchmark cases
specified in the comma-separated values ~definitions.csv~ file in Listing
[[definitions]]. The latter features six columns:

1. =system_type= indicating the type of linear system (=coupled= or =fembem=),
2. =solver= specifying the solver to use (=hmat=, =mumps= or =mumps/hmat=),
3. =threads= giving the number of threads to use for computation,
4. =nbpts= giving the number of unknowns in the system, i.e. the size of the
   system,
5. =schur= setting Schur complement block dimension in case of coupled solver
   benchmarks,
6. =arith= defining the arithmetic and precision of matrix coefficients (=s= for
   simple real, =d= for double real, =c= for simple complex or =z= for double
   complex),
7. =sym= indicating the symmetry of the coefficient matrix (=1= for symmetric or
   =0= for non-symmetric).

#+CAPTION: ~definitions.csv~ file defining benchmark cases to run within the
#+CAPTION: experimental part of the study.
#+NAME: definitions
#+HEADER: :tangle definitions.csv :eval no
#+BEGIN_SRC text
system_type,solver,threads,nbpts,schur,arith,sym
coupled_mf,mumps_hmat,24,10000,1776,z,1
coupled_mf,mumps_hmat,24,25000,3200,z,1
coupled_mf,mumps_hmat,24,50000,5040,z,1
coupled_mf,mumps_hmat,24,100000,8109,z,1
coupled_mf,mumps_hmat,24,250000,14835,z,1
coupled_mf,mumps_hmat,24,500000,23577,z,1
coupled_mf,mumps_hmat,24,1000000,37169,z,1
coupled_ms,mumps_hmat,24,10000,0,z,1
coupled_ms,mumps_hmat,24,25000,0,z,1
coupled_ms,mumps_hmat,24,50000,0,z,1
coupled_ms,mumps_hmat,24,100000,0,z,1
coupled_ms,mumps_hmat,24,250000,0,z,1
coupled_ms,mumps_hmat,24,500000,0,z,1
coupled_ms,mumps_hmat,24,1000000,0,z,1
coupled_ms,mumps_hmat,24,2000000,0,z,1
fembem,hmat,24,10000,0,z,1
fembem,hmat,24,25000,0,z,1
fembem,hmat,24,50000,0,z,1
fembem,hmat,24,100000,0,z,1
fembem,hmat,24,250000,0,z,1
fembem,hmat,24,500000,0,z,1
fembem,hmat,24,1000000,0,z,1
fembem,hmat,24,2000000,0,z,1
fembem,mumps,24,10000,0,z,1
fembem,mumps,24,25000,0,z,1
fembem,mumps,24,50000,0,z,1
fembem,mumps,24,100000,0,z,1
fembem,mumps,24,250000,0,z,1
fembem,mumps,24,500000,0,z,1
fembem,mumps,24,1000000,0,z,1
fembem,mumps,24,2000000,0,z,1
#+END_SRC

We then pass the ~definitions.csv~ file to the ~run.sh~ shell script (see
Section [[#run]]) we use to execute all the benchmarks.
